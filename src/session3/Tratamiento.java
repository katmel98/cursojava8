package session3;

interface Tratamiento {
	
	String ORDENANTE = "SEGURIDAD SOCIAL";

	public void asignar(String nombre);
	public void aplicar();
	public void revisar();
	
}
