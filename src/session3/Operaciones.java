package session3;

public class Operaciones {

	interface Matematicas {
		public int operacion(int x, int y);
	}
	
	interface Saludos {
		public void hola(String persona1, String persona2);
	}
	
//	private class Suma implements Matematicas {
//		public int operacion(int x, int y) {
//			return x + y;
//		}
//	}
	
	public void realizarCalculo() {
//		Suma nueva = new Suma();
//		System.out.println(nueva.operacion(3,2));
		
		Matematicas suma = (x, y) -> x + y;
		System.out.println(suma.operacion(3,2));
		
		Matematicas resta = (x, y) -> x - y;
		System.out.println(resta.operacion(3,2));
		
		Saludos quetal = (x, y) -> {
			System.out.println("Hola " + x);
			System.out.println("Hola " + y);
		};
		quetal.hola("Mimí","Oriol");
		
		
		
		
	}
	
	public static void main(String[] args) {

		Operaciones oper = new Operaciones();
		oper.realizarCalculo();
		
	}

}
