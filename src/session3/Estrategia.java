package session3;

@FunctionalInterface
interface Estrategia {
	
	public String saludos(String persona);

	default String despedida() {
		return "Hasta luego";
	}
}
