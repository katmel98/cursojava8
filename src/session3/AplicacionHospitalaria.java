package session3;

public class AplicacionHospitalaria {

	public static void main (String[] args)  {
		
		Paciente nuevo = new Paciente();
		
		nuevo.setNombre("Carlos");
		nuevo.asignar("Colesterolemia");
		
		System.out.println(Tratamiento.ORDENANTE);
		
		Medico house = new Medico();
		house.setNombre("Gregory");
		house.saludos();
		house.ordenarAnalitica(nuevo);
		
		MedicoEspecialista.hola();
		
		System.out.println(MedicoCabecera.PESO);
		System.out.println(MedicoEspecialista.PESO);
		
		
	}
	
}
