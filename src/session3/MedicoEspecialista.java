package session3;

interface MedicoEspecialista {
	
	String TIPO = "Especialista";
	int PESO = 20;
	
	static void hola() {
		System.out.println("HOLA, SOY MEDICO ESPECIALISTA");
	}
	
	public void ordenarAnalitica(Paciente x);
//	public void ordenarPrueba(Paciente x);

}
