package session3;

import java.util.ArrayList;
import java.util.List;

public class EjemploLambda {
	
	public static void main(String[] args) {
		
		List<UserModel> userModels = getUsers();
		
		userModels.sort((UserModel u1, UserModel u2) -> u1.getPosicion().compareTo(u2.getPosicion()));

		userModels.forEach((userModel)->System.out.println(userModel.getNombre()));
		
		userModels.forEach(user -> {
			if("Luis".equals(user.getNombre())) {
				System.out.println("Hola Luis");
			} else {
				System.out.println(user.getNombre());
			}
		});
	}
	
	static class UserModel {
		
		String nombre;
		String posicion;
		
		public UserModel(String nombre, String posicion) {
			this.nombre = nombre;
			this.posicion = posicion;
		}
		
		public String getNombre() {
			return this.nombre;
		}
		
		public String getPosicion() {
			return this.posicion;
		}

	}
	
	static List<UserModel> getUsers() {
		
		List<UserModel> userModels = new ArrayList<>();
		userModels.add(new UserModel("David", "A"));
		userModels.add(new UserModel("Pedro", "Z"));
		userModels.add(new UserModel("Luis", "G"));
		
		return userModels;
		
		
	}

}
