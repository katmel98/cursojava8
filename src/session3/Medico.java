package session3;

public class Medico extends Persona implements MedicoCabecera {
	
	private String especialidad;
	
	Medico(){}
	
	Medico(String especialidad) {
		this.especialidad = especialidad;
	}
	
	public void ordenarAnalitica(Paciente x) {
		System.out.println("El médico " + this.getNombre() + " le ha ordenado una analítica a el paciente " + x.getNombre());
	}
	
	private class Estudios {
		public void verEstudios() {
			if(especialidad== "CARDIOLOGO") {
				System.out.println("Estudió la carrera de Cardiología");
			} else if(especialidad=="NEFROLOGO") {
				System.out.println("Estudió la carrera de Nefrología");
			} else {
				System.out.println("Hay que investigar que estudió este sujeto!!!!");
			}
		}
	}
	
	public void mostrarEstudios() {
		Estudios e = new Estudios();
		e.verEstudios();
	}
	
	public void getExperiencia() {
		int tiempoExperiencia = 20;
		class Experiencia {
			public void verExperiencia() {
				System.out.println("La experiencia del medico es de " + tiempoExperiencia + " años.");
			}
		}
		Experiencia exp = new Experiencia();
		exp.verExperiencia();
	}

}
