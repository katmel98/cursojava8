package session3;

public class Persona {
	
	private String nombre;
	private int edad;
	
	Persona() {
		
	}
	
	Persona(String nombre){
		this.nombre = nombre;
	}
	
	Persona(String nombre, int edad){
		this.nombre = nombre;
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}
	
	public int getEdad() {
		return edad;
	}
	
	public void setNombre(String nuevoNombre) {
		nombre = nuevoNombre;
	}

	public void setEdad(int nuevaEdad) {
		edad = nuevaEdad;
	}
	
	public void sentir() {
		System.out.println("Estoy pasando un buen día");
	}
	
}
