package session3;

interface MedicoCabecera {
	
	String TIPO = "Cabecera";
	int PESO = 10;
	
	public void ordenarAnalitica(Paciente x);
	
	default void saludos() {
		System.out.println("Saludo como Médico de Cabecera");
	};

}
