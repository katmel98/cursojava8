package session2;

public class Paciente extends Persona{
	
	private int numeroSeguridadSocial;
	private String afeccion;
	private String ubicacion;
	private static String unidad;
	
	Paciente(){
		
	}
	
	Paciente(String nombre, int edad){
		super(nombre, edad);
	}
	
	Paciente(int nSS, String afeccion, String ubicacion){
		setNumeroSeguridadSocial(nSS);
		setAfeccion(afeccion);
		setUbicacion(ubicacion);		
	}
	
	public int getNumeroSeguridadSocial() {
		return numeroSeguridadSocial;
	}
	
	public void setNumeroSeguridadSocial(int nSS) {
		this.numeroSeguridadSocial = nSS;
	}

	public String getAfeccion() {
		return afeccion;
	}

	public void setAfeccion(String afeccion) {
		this.afeccion = afeccion;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String nuevaUbicacion) {
		this.ubicacion = nuevaUbicacion;
	}
	
	public void sentir() {
		if (afeccion == null || afeccion.isEmpty()) {
			System.out.println("Me siento fenomenal");
		} else {
			System.out.println("Debido a " + this.afeccion + ", no me siento bien");			
		}
	}
	
	public static class Cardiaco {
		
		public Cardiaco() {
			unidad = "CARDIOLOGIA";
			System.out.println("Se ha creado a un paciente cardíaco, en la unidad " + unidad);
		}
		
	}
	
}
