package session2;

public class AplicacionHospitalaria {

	public static void main (String[] args)  {
		
		// Polimorfismo
		
		Persona paciente1 = new Paciente();
				
		paciente1.setNombre("Pedro Perez");
		
		paciente1.sentir();
		System.out.println(paciente1.getNombre());
		
		// Clases estáticas
		
		Paciente.Cardiaco paciente2 = new Paciente.Cardiaco();
		
		// Clases abstractas
		
		Enfermera e = new Enfermera("Maria");
		
		e.cuidar("Mario");
		e.funcion();
		
		Familiar f = new Familiar("Carlos");
		f.cuidar("Jose");
		
		// Clases anidadas
		
		Medico m = new Medico("DENTISTA");
		m.mostrarEstudios();
		
		// Clases anidadas en metodos
		
		Medico m1 = new Medico("TESTER");
		m1.getExperiencia();
		
	}
	
}
