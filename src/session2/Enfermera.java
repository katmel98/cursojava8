package session2;

public class Enfermera extends Cuidador{
	
	Enfermera() {
		
	}
	
	Enfermera(String valor){
		nombre = valor;
	}
	
	public void cuidar(String paciente) {
		System.out.println("La enfermera: " + nombre + ", cuida a " + paciente );
	}

}
