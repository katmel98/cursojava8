package session2;

public class Familiar extends Cuidador {
	
	Familiar(String valor){
		nombre = valor;
	}
	
	public void cuidar(String paciente) {
		System.out.println(nombre + " es familiar y cuida a " + paciente);
	}

}
