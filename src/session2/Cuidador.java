package session2;

abstract class Cuidador {

	public String nombre;
	
	public void funcion() {
		System.out.println("Mi funcion es cuidar a un(a) paciente");
	}
	
	public abstract void cuidar(String paciente);
	
}
