package session2;

public class Medico {
	
	private String especialidad;
	
	Medico(String especialidad){
		this.especialidad = especialidad;
	}

	private class Estudios {
		public void imprimir() {
			if(especialidad == "CARDIOLOGO") {
				System.out.println("Estudio Cardiologia");
			}else if(especialidad == "NEFROLOGO"){
				System.out.println("Estudio Nefrologia");
			} else {
				System.out.println("Hay que investigar que estudio este sujeto!!!");
			}
			
		}
	}
	
	public void mostrarEstudios() {
		Estudios e = new Estudios();
		e.imprimir();
	}
	
	public void getExperiencia() {
		int tiempoExperiencia = 20;
		
		class Experiencia {
			public void imprimirExperiencia() {
				System.out.println("La expriencia del doctor es de " + tiempoExperiencia + " años.");
			}
		}
		
		Experiencia exp = new Experiencia();
		exp.imprimirExperiencia();
	}
	
}
