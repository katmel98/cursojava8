package edu.java8course.stream.avanzados;

public class Employee {
	
	private Integer id;
	private String name;
	private Double salary;
	
	Employee(Integer id, String name, Double salary) {
		this.id = id;
		this.setName(name);
		this.salary = salary;
	}

	public void salaryIncrement(double increment) {
		
		this.salary = this.salary + ((this.salary*increment)/100);
		
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}
	
}
