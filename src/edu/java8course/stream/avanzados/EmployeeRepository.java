package edu.java8course.stream.avanzados;

import java.util.List;

public class EmployeeRepository {
	
	private List<Employee> employeesList;
	
	public Employee findById(Integer id) {
		
		List<Employee> mylist = this.employeesList;
				
		Employee val = mylist.stream()
			.filter(e -> id.equals(e.getId()))
			.findAny()
			.orElse(null);
		
		return val;
		
	}

	public List<Employee> getEmployeesList() {
		return employeesList;
	}

	public void setEmployeesList(List<Employee> employeesList) {
		this.employeesList = employeesList;
	}

}
