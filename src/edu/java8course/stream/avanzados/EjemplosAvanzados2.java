package edu.java8course.stream.avanzados;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EjemplosAvanzados2 {
	
	private static Employee[] arrayOfEmps = {
	    new Employee(1, "Jeff Bezos", 100000.0), 
	    new Employee(2, "Bill Gates", 200000.0), 
	    new Employee(3, "Mark Zuckerberg", 300000.0)
	};
	
	private static List<Employee> empList = Arrays.asList(arrayOfEmps);

	public static void main(String[] args) {
		
		// EJEMPLO FOREACH
		empList.stream().forEach(e -> e.salaryIncrement(10.0));
		empList.forEach(e -> System.out.println(e.getSalary()));
		
		Integer[] empIds = {1, 2, 3, 4};
		EmployeeRepository employeeRepository = new EmployeeRepository();
		employeeRepository.setEmployeesList(empList);

		// EJEMPLO MAP
//		
//		List<Employee> employees = Stream.of(empIds)
//				.map(employeeRepository::findById)
//				.collect(Collectors.toList());
//		
//		employees.forEach(e -> {
//			System.out.println("ID: " + e.getId() + ", Name: " + e.getName() + ", SALARY: " + e.getSalary());
//		});
		
		// EJEMPLO FILTER
		List<Employee> employees = Stream.of(empIds)
			      .map(employeeRepository::findById)
			      .filter(e -> e != null)
			      .filter(e -> e.getSalary() > 200000)
			      .collect(Collectors.toList());
		
		employees.forEach(e -> {
			System.out.println("ID: " + e.getId() + ", Name: " + e.getName() + ", SALARY: " + e.getSalary());
		});
		
		// EJEMPLO FINDFIRST
	    Employee employee = Stream.of(empIds)
	    	      .map(employeeRepository::findById)
	    	      .filter(e -> e != null)
	    	      .filter(e -> e.getSalary() > 100000)
	    	      .findFirst()
	    	      .orElse(null);
	    	    
	    System.out.println("ID: " + employee.getId() + ", Name: " + employee.getName() + ", SALARY: " + employee.getSalary());
	    
	    // EJEMPLO FLAT_MAP
	    List<List<String>> namesNested = Arrays.asList( 
	    	      Arrays.asList("Jeff", "Bezos"), 
	    	      Arrays.asList("Bill", "Gates"), 
	    	      Arrays.asList("Mark", "Zuckerberg"));

	    List<String> namesFlatStream = namesNested.stream()
	      .flatMap(Collection::stream)
	      .collect(Collectors.toList());

	    System.out.println(namesFlatStream);
	    
	    // EJEMPLO PEEK
	    
	    empList.stream()
	      .peek(e -> e.salaryIncrement(10.0))
	      .peek(e-> System.out.println(e.getName()))
	      .collect(Collectors.toList());

	    // SHORT CIRCUITING OPERATIONS
	    
	    Stream<Integer> infiniteStream = Stream.iterate(2, i -> i * 2);

	    List<Integer> collect = infiniteStream
	      .skip(3)
	      .limit(5)
	      .collect(Collectors.toList());
	    
	    System.out.println(collect);
	    
	    // REDUCE
	    Double sumSal = empList.stream()
	    	      .map(Employee::getSalary)
	    	      .reduce(0.0, Double::sum);
	    
	    System.out.println(sumSal);

	    
	}

}
