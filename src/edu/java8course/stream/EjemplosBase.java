package edu.java8course.stream;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class EjemplosBase {

	public static void main(String[] args) {

		// Ejemplo base
		List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
		List<String> filtered = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
		
		System.out.println(filtered);
		
		// EJEMPLO FOREACH + LIMIT
		Random random = new Random();
		random.ints().limit(10).forEach(System.out::println);
		
		// EJEMPLO MAP
		List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
		//get list of unique squares
		List<Integer> squaresList = numbers.stream().map( i -> i*i).distinct().collect(Collectors.toList());
		System.out.println(squaresList);

		// EJEMPLO FILTER
		List<String>strings2 = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
		//get count of empty string
		int count = (int)strings2.stream().filter(string -> string.isEmpty()).count();
		System.out.println(count);
		
		// EJEMPLO SORTED
		Random random2 = new Random();
		random2.ints().limit(10).sorted().forEach(System.out::println);
		
		// EJEMPLO COLLECTORS
		List<String>strings3 = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
		List<String> filtered3 = strings3.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());

		System.out.println("Filtered List: " + filtered3);
		String mergedString = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.joining(", "));
		System.out.println("Merged String: " + mergedString);
		
		// EJEMPLO STATISTICS
		List<Integer> integers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);

		IntSummaryStatistics stats = integers.stream().mapToInt((x) -> x).summaryStatistics();

		System.out.println("Highest number in List : " + stats.getMax());
		System.out.println("Lowest number in List : " + stats.getMin());
		System.out.println("Sum of all numbers : " + stats.getSum());
		System.out.println("Average of all numbers : " + stats.getAverage());
		
	}

}
