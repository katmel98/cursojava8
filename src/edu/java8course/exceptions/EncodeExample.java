package edu.java8course.exceptions;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class EncodeExample {

	public static void main(String[] args) {

//		List<String> myList =
//			    Arrays.asList("https://www.gmail.com", "https://www.yahoo.com", "https://loopback.io/?id='urhfhdd sjsjsjsj'");
		
		List<String> result = Arrays.asList(
				encodedAddressUsingTryCatch(
					"www.gmail.com", 
					"www.yahoo.com", 
					"loopback.io/?id='urhfhdd sjsjsjsj'"
					)
				);
		
		System.out.println(result);
		
		
	}

	public static String encodeAddressAnonInnerClass(String... values) {
	    return Arrays.stream(values)
	        .map(new Function<String, String>() {
	            @Override
	            public String apply(String s) {
	                try {
	                    return URLEncoder.encode(s, "UTF-8");
	                } catch (UnsupportedEncodingException e) {
	                    e.printStackTrace();
	                    return "";
	                }
	            }
	        })
	        .collect(Collectors.joining(","));
	}	
	
	public static String encodedAddressUsingTryCatch(String... address) {
	    return Arrays.stream(address)
	                .map(s -> {
	                    try {
	                        return URLEncoder.encode(s, "UTF-8");
	                    } catch (UnsupportedEncodingException e) {
	                        throw new RuntimeException(e);
	                    }
	                })
	                .collect(Collectors.joining(","));
	}	
}
