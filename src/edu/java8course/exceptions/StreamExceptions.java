package edu.java8course.exceptions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamExceptions {
	
	public static void main(String[] args) {
		
		List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
		
		List<Integer> scales = scale3(numbers, 2);
		
		System.out.println(scales);
		
	}
	
	private static List<Integer> scale1(List<Integer> values, Integer factor) {
	    return values.stream()
	        .map(n -> n / factor)
	        .collect(Collectors.toList());
	}
	
	private static List<Integer> scale2(List<Integer> values, Integer factor) {
	    return values.stream()
	        .map(n -> {
	        	Integer val = 0;	        	
	        	try{
	        		val = n / factor;
	        	}catch(ArithmeticException e) {
	        		e.printStackTrace();
	        	}
				return val;
	        })
	        .collect(Collectors.toList());
	}

	private static List<Integer> scale3(List<Integer> values, Integer factor) {
	    return values.stream()
	        .map(n -> divide(n,factor))
	        .collect(Collectors.toList());
	}

	private static Integer divide(Integer value, Integer factor) {
	    Integer val = 0;
		try {
	        val = value / factor;
	    } catch (ArithmeticException e) {
	        e.printStackTrace();
	    }
		return val;
	}

}
