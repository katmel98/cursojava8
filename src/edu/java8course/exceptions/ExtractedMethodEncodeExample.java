package edu.java8course.exceptions;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ExtractedMethodEncodeExample {

	public static void main(String[] args) {


			List<String> result = Arrays.asList(
					encodedAddressUsingTryCatch(
						"www.gmail.com", 
						"www.yahoo.com/?id='æ¨w'", 
						"loopback.io/?id='urhfhdd sjsjsjsj'",
						""
						)
					);
			
			System.out.println(result);
			
			
	}
	
	private static String encodeString(String s) {
	    try {
	        return URLEncoder.encode(s, "UTF-8");
	    } catch (NullPointerException e) {
	        e.printStackTrace();
	    } catch (UnsupportedEncodingException e) {
	        e.printStackTrace();
	    }
	    return s;
	}

	public static String encodedAddressUsingTryCatch(String... address) {
	    return Arrays.stream(address)
	            .map(ExtractedMethodEncodeExample::encodeString)
	            .collect(Collectors.joining(","));
	}

}
