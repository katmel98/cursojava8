package edu.java8course.exceptions;
import java.util.Scanner; 

public class AssertionExample {

	// Java program to demonstrate syntax of assertion 
	  
    public static void main( String args[] ) 
    { 
        int value = 15; 
        assert value >= 20 : " Underweight"; 
        System.out.println("value is "+value); 
    } 

}
