package edu.java8course.exceptions;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GeneralMethodEncodeExample {
	
	public static void main(String[] args) {
		
		List<String> result = Arrays.asList(
				encodedAddressUsingWrapper(
					"www.gmail.com", 
					"www.yahoo.com/?id='æ¨w'", 
					"loopback.io/?id='urhfhdd sjsjsjsj'",
					""
					)
				);
		
		System.out.println(result);
		
	}
	
	private static <T, R, E extends Exception> Function<T, R> wrapper(FunctionWithException<T, R, E> fe) {
		return arg -> {
			try {
				return fe.apply(arg);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
	}
	
	public static String encodedAddressUsingWrapper(String... address) {
	    return Arrays.stream(address)
	            .map(wrapper(s -> URLEncoder.encode(s, "UTF-8")))
	            .collect(Collectors.joining(","));
	 }

}
