package edu.java8course.localization;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;

public class TestExample {

	public static void main(String[] args) {
		
		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		System.out.println("Current relative path is: " + s);

		// en_US
		System.out.println("Current Locale: " + Locale.getDefault());
		ResourceBundle mybundle = ResourceBundle.getBundle("edu.java8course.localization.MyLabels");

		// read MyLabels_en_US.properties
		System.out.println("Say how are you in ES spanish: " + mybundle.getString("how_are_you"));

		Locale.setDefault(new Locale("en", "US"));

		// read MyLabels_ms_MY.properties
		System.out.println("Current Locale: " + Locale.getDefault());
		mybundle = ResourceBundle.getBundle("edu.java8course.localization.MyLabels");
		System.out.println("Say how are you in US english: " + mybundle.getString("how_are_you"));

	}
	
}
