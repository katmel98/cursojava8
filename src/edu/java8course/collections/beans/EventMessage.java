package edu.java8course.collections.beans;

import java.util.Date;

public class EventMessage extends Message {

	private String source;
	
	private Date createdDate;
	
	public EventMessage(int id, String type, String content, String source, Date createdDate) {
		super(id, type, content);
		this.source = source;
		this.createdDate = createdDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Override
	public String toString() {
		String result = super.toString()+" [EventMessage]:"+this.getSource()+", "+this.getCreatedDate();
		return result;
	}
	
}
