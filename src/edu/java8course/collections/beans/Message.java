package edu.java8course.collections.beans;

public abstract class Message {
	
	private int id;
	
	private String type;
	
	private String content;

	public Message(int id, String type, String content) {
		super();
		this.id = id;
		this.type = type;
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", type=" + type + ", content=" + content + "]";
	}
	
}
