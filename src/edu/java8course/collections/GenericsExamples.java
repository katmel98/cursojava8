package edu.java8course.collections;

import java.util.Date;

import edu.java8course.collections.beans.EventMessage;
import edu.java8course.collections.beans.PushMessage;
import edu.java8course.collections.services.MessageService;
import edu.java8course.collections.services.xml.EventMessageXmlService;
import edu.java8course.collections.services.xml.PushMessageXmlService;

public class GenericsExamples {

	public static void main(String [] args) {
		MessageService<PushMessage> pushMessageService = new PushMessageXmlService();
		MessageService<EventMessage> eventMessageService = new EventMessageXmlService();
		
		pushMessageService.notify(new PushMessage(1, "A", "First push message", "iOS"));
		pushMessageService.notify(new PushMessage(2, "A", "Second push message", "Windows phone"));
		
		eventMessageService.notify(new EventMessage(3, "001", "First event message", "RabbitMQ", new Date()));
		eventMessageService.notify(new EventMessage(4, "00104", "Second event message", "Kafka", new Date()));
	}

}
