package edu.java8course.collections.services;

import edu.java8course.collections.beans.PushMessage;

public abstract class AbstractPushMessageService implements MessageService<PushMessage> {

	public AbstractPushMessageService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void notify(PushMessage message) {
		// TODO Auto-generated method stub
		System.out.println(message.toString());
	}

	@Override
	public void notifyAll(PushMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listen(PushMessage message) {
		// TODO Auto-generated method stub
		
	}

}
