package edu.java8course.collections.services;

import edu.java8course.collections.beans.Message;

public interface MessageService<T extends Message> {
	
	void notify(T message);
	
	void notifyAll(T message);
	
	void listen(T message);

}
