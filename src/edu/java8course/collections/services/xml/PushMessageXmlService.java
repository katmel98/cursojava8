package edu.java8course.collections.services.xml;

import edu.java8course.collections.beans.PushMessage;
import edu.java8course.collections.services.AbstractPushMessageService;

public class PushMessageXmlService extends AbstractPushMessageService {

	public PushMessageXmlService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void notify(PushMessage message) {
		super.notify(message);
		System.out.println("Processing XML...");
	}
	
}
