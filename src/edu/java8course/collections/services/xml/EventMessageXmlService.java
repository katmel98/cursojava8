package edu.java8course.collections.services.xml;

import edu.java8course.collections.beans.EventMessage;
import edu.java8course.collections.services.AbstractEventMessageService;

public class EventMessageXmlService extends AbstractEventMessageService {

	public EventMessageXmlService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void notify(EventMessage message) {
		super.notify(message);
		System.out.println("Processing XML...");
	}
	
}
