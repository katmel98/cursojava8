package edu.java8course.methodreference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

public class ConstructorMethodReference {
	
    public static void main(String args[]) {
        final List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        System.out.println("Old List: " + list);
        // Method Reference
        copyElements(list, ArrayList<Integer>::new);
        // Lambda expression
        copyElements(list, () -> new ArrayList<Integer>());
        
    }
    
    private static void copyElements(final List<Integer> list, final Supplier<Collection<Integer>> targetCollection) {
        // Method reference to a particular instance
    	Collection<Integer> a = targetCollection.get();
//    	list.forEach(col -> {
//    	a.add(col);
//    });
    	
    	list.forEach(a::add);
        
        System.out.println("New List: " + a);
    }
}
