package edu.java8course.io;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Nio2Test {
	
	public static void main(String[] args) {
		
		Path p1 = Paths.get("/Users/katmel98/start_oddo.sh");
		
		Path p2 = Paths.get("/Users", "katmel98/start_oddo.sh");
		
		System.out.println(p2);
	
		Path p3 = Paths.get("input.txt");
		
		System.out.println(p3);
		
		
		Path path = Paths.get("/Users/katmel98/start_oddo.sh"); 
		System.out.println(path);
//		Path path = path.getFileName(); // returns file.txt
//		Path path = path.getName(0); // returns users
//		int count = path.getNameCount(); // returns 3 (users, admin and file.txt)
//		Path path = path.subpath(0, 2); // returns users/admin
//		Path path = path.getParent(); // returns /users/admin
//		Path path = path.getRoot(); // 
		
		
//		Path path = Paths.get("input.txt");
		try {
		    List<String> lines = Files.readAllLines(path);
		    
		    System.out.println(lines);
		} catch (IOException e) {
		    // something failed
		    e.printStackTrace();
		}

		Path source  = Paths.get("input.txt");
		Path target = Paths.get("input-copy.txt");

		try {
		    Files.copy(source, target);
		} catch(FileAlreadyExistsException fae) {
		    fae.printStackTrace();
		} catch (IOException e) {
		    // something else went wrong
		    e.printStackTrace();
		}

		
	}

	
	
	
}
