package edu.java8course.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Nio2Stream {

	public static void main(String[] args) {

		try (Stream<Path> stream = Files.list(Paths.get("/Users/katmel98/Downloads"))) {
		    stream.map(String::valueOf)
		        .filter(path -> path.endsWith(".pdf"))
		        .forEach(System.out::println);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
