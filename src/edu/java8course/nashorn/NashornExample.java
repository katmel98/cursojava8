package edu.java8course.nashorn;

import java.io.FileReader;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

public class NashornExample {
	 
    private static final String NASHORN_ENGINE_NAME = "nashorn";
 
    private final ScriptEngine engine;
 
    private Invocable invoker;
 
    public static void main(final String[] args) throws Exception {
        final NashornExample nashornExample = new NashornExample();
        nashornExample.example01();
        nashornExample.example02();
        nashornExample.example03();
        nashornExample.example04();
        nashornExample.example05();
        nashornExample.example06();

    }
 
    public NashornExample() {
        final ScriptEngineManager manager = new ScriptEngineManager();
        engine = manager.getEngineByName(NASHORN_ENGINE_NAME);
        invoker = (Invocable)engine;
    }
 
    public void example01() throws Exception {
        engine.eval("print('Hola mundo')");
    }
 
    public void example02() throws Exception {
        engine.eval(new FileReader("example02.js"));
    }
 
    public void example03() throws Exception {
 
        engine.eval(new FileReader("example03.js"));
        // la siguiente linea invoca la función "greeter" con parámetro "Autentia"
        final String greetingMessage = (String)invoker.invokeFunction("greeter", "Autentia");
        System.out.println(greetingMessage);
    }
    
    public void example04() throws Exception {
        engine.eval(new FileReader("example04.js"));
        final Object vehiculos = engine.get("vehiculos");
        final String[] vehiculosSeleccionados = ((ScriptObjectMirror)invoker.invokeMethod(vehiculos, "slice", 0, 2)).to(String[].class);
     
        System.out.println("Vehiculos seleccionados: ");
        for (final String vehiculo : vehiculosSeleccionados) {
            System.out.println("* " + vehiculo);
        }
    }
    
    public void example05() throws Exception {
        engine.eval(new FileReader("finance.js"));
        engine.eval("var finance = new Finance();");
        final Object finance = engine.get("finance");
     
        // calcula la cuota mensual de un préstamo de 100.000€ al 5.5% anual durante 30 años
        // (360 meses, ya que el último parámetro indica que este valor está expresado en meses)
        final double cuotaMensual = (Double)invoker.invokeMethod(finance, "AM", 100000, 5.5, 360, 1);
     
        System.out.println("La cuota mensual es: " + cuotaMensual);
    }
    
    public void example06() throws Exception {
        engine.eval(new FileReader("example06.js"));
        final Object runnerjs = engine.get("runnerImpl");
        final Runnable runner = invoker.getInterface(runnerjs, Runnable.class);
     
        final Thread thread = new Thread(runner);
     
        thread.start();
        thread.join();
    }
    
    
    
}