package edu.java8course.forkjoin;

import java.util.concurrent.RecursiveAction;

public class IncrementFork extends RecursiveAction {

	public static final int LIMIT = 10;
	
	private String name;
	
	private int[] nums;
	
	private long initialTime;
	
	private int start;
	
	private int last;
	
	private int total;
	
	public IncrementFork(String name, int[] nums, long initialTime, int start, int last) {
		super();
		this.name = name;
		this.nums = nums;
		this.initialTime = initialTime;
		this.start = start;
		this.last = last;
	}

	public int getTotal() {
		return total;
	}

	@Override
	protected void compute() {
		if ((this.last - this.start) < LIMIT) {
			//Sequencial work.
			this.sum();
		}
		else {
			//Parallel work.			
			int middle = (this.start + this.last) / 2;

			IncrementFork if1 = new IncrementFork("", this.nums, this.initialTime, this.start, middle + 1);
			IncrementFork if2 = new IncrementFork("", this.nums, this.initialTime, middle + 1, this.last);
			
			invokeAll(if1, if2);
			
			this.total = if1.getTotal() + if2.getTotal();
		}
	}

	private void sum() {
		for (int i = this.start; i < this.last; i++) {
			this.total += this.nums[i];
		}
	}
	
}
