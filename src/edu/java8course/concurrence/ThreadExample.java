package edu.java8course.concurrence;

public class ThreadExample {

	public static void main(String[] args) {
		long initialTime = System.currentTimeMillis();
		
		System.out.println("### Thread Example ###");
		
		IncrementThread it1 = new IncrementThread("it1", new int[] {2,2,3,3,4,4}, initialTime);
		IncrementThread it2 = new IncrementThread("it2", new int[] {2,5,3,5,4,5}, initialTime);
		
		it1.start();
		it2.start();
		
		System.out.println("### Runnable Example ###");
		
		IncrementRunnable ir1 = new IncrementRunnable("ir1", new int[] {2,2,3,3,4,4}, initialTime);
		IncrementRunnable ir2 = new IncrementRunnable("ir2", new int[] {2,5,3,5,4,5}, initialTime);
		
		new Thread(ir1).start();
		new Thread(ir2).start();
	}

}
