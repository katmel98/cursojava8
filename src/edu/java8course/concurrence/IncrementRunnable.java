package edu.java8course.concurrence;

public class IncrementRunnable implements Runnable {

	private String name;
	
	private int [] nums;
	
	private long initialTime;
	
	private int total;

	public IncrementRunnable(String name, int[] nums, long initialTime) {
		super();
		this.name = name;
		this.nums = nums;
		this.initialTime = initialTime;
	}
	
	@Override
	public void run() {
		for (int i : nums) {
			this.total += i;
			System.out.println("["+this.getClass().getName()+":"+this.name+"]"+"- ms de iteración: "+(System.currentTimeMillis()-this.initialTime));
			try {
				Thread.sleep(100);
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("["+this.getClass().getName()+":"+this.name+"] Total = "+this.total);
	}

}
