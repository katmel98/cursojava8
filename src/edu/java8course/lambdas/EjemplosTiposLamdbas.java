package edu.java8course.lambdas;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EjemplosTiposLamdbas {

	public static void main(String[] args) {
		
		// EJEMPLO CONSUMER
		Consumer<Product> updatePrice = p -> p.setPrice(5.9);
	    Product p = new Product();
	    updatePrice.accept(p);
	    p.printPrice();		
	    		
		// EJEMPLO SUPPLIER 1
		int n = 3;
		display(() -> n + 10);
		display(() -> n + 100);
		
		// EJEMPLO SUPPLIER 2
		
        List<Persona> lista=Stream.generate(Persona::new)
                .limit(10)
                .peek((person)->person.setNombre("pepe"))
                .collect(Collectors.toList());
 
         
        for (Persona person: lista) {
            System.out.println(person.getNombre());
        }
        
        // EJEMPLO FUNCTION 1 
        int x = 5;
        modifyValue(x, val-> val + 10);
        modifyValue(x, val-> val * 100);
        
        // EJEMPLO FUNCION 2
        List<String> strings = Arrays.asList("hola","que","tal");
        strings.stream()
        	.map((string)->string.toUpperCase())
        	.forEach((string)->System.out.println(string));

		
		// EJEMPLO PREDICATE NORMAL
		Predicate<String> isALongWord = new Predicate<String>() {
		    @Override
		    public boolean test(String t) {
		        return t.length() > 10;
		    }
		};
		String s = "successfully";
		boolean result = isALongWord.test(s);
		System.out.println(result);


		// EJEMPLO PREDICATE LAMBDA
		Predicate<String> isALongWordLambda = t -> t.length() > 10;
		String s2 = "unsuccessfully";
		boolean result2 = isALongWordLambda.test(s);
		System.out.println(result2);
		
		// EJEMPLO OPERADORES
        List<String> strings2=Arrays.asList("hola","que","tal");
        Optional<String> resultado=strings2.stream().reduce(String::concat);
        if(resultado.isPresent()) {
             
            System.out.println(resultado.get());
        }
		
		

		
		
		
	}
	
	static void display(Supplier<Integer> arg) {
		System.out.println(arg.get());
	}
	
	static void modifyValue(int v, Function<Integer, Integer> function){
		System.out.println("ESTOY AQUI");
		int result = function.apply(v);
		System.out.println(result);
	}


}
