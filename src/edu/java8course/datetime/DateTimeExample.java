package edu.java8course.datetime;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DateTimeExample {

	public static void main(String[] args) {

		// Calendar deprecated
			Calendar calendar = new GregorianCalendar(2014, 2, 18);
			System.out.println(calendar.getTime());
		// ******** **********
		
		// **** LocalDate ****
			LocalDate date = LocalDate.of(1989, 11, 11); //1989-11-11
			System.out.println(date.getYear()); //1989
			System.out.println(date.getMonth()); //NOVEMBER
			System.out.println(date.getDayOfMonth()); //11
			// USING ENUM MONTH
			date = LocalDate.of(1989, Month.NOVEMBER, 11);
			System.out.println(date);
			// GETTING NOW DATE
			date = LocalDate.now();
			System.out.println(date);
		// **** ********* ****
		
		// **** LocalTime ****
			LocalTime time = LocalTime.of(5, 30, 45, 35); //05:30:45:35
			System.out.println(time.getHour()); //5
			System.out.println(time.getMinute()); //30
			System.out.println(time.getSecond()); //45
			System.out.println(time.getNano()); //35
			
			// GETTING NOW TIME
			time = LocalTime.now();
			System.out.println(time);
		// **** ********* ****
		

		// **** LocalDateTime ****
			LocalDateTime dateTime = LocalDateTime.of(1989, 11, 11, 5, 30, 45, 35); //1989-11-11T05:30:45.000000035
			System.out.println(dateTime);
		
			date = LocalDate.of(1989, 11, 11);
			time = LocalTime.of(5, 30, 45, 35);
			dateTime = LocalDateTime.of(date, time);		
			System.out.println(date);
			System.out.println(time);
			System.out.println(dateTime);
	
			// Getting NOW DateTime
			dateTime = LocalDateTime.now();
			System.out.println(dateTime);
		// **** ********* ****
		
		// INSTICT CLASS
			Instant instant = Instant.ofEpochSecond(120);
			System.out.println(instant);
			
			instant = Instant.now();
			System.out.println(instant);
		// ******* *****
			
		// DURATION
			LocalTime localTime1 = LocalTime.of(12, 25);
			LocalTime localTime2 = LocalTime.of(17, 35);
			Duration duration = Duration.between(localTime1, localTime2);
			System.out.println(duration);
			
			LocalDateTime localDateTime1 = LocalDateTime.of(2016, Month.JULY, 18, 14, 13);
			LocalDateTime localDateTime2 = LocalDateTime.of(2016, Month.JULY, 20, 12, 25);
			duration = Duration.between(localDateTime1, localDateTime2);			
			System.out.println(duration);
			
			Duration oneDayDuration = Duration.of(1, ChronoUnit.DAYS);
			System.out.println(oneDayDuration);
			
			oneDayDuration = Duration.ofDays(1);
			System.out.println(oneDayDuration);
		// ********
			
		// PERIOD
			LocalDate localDate1 = LocalDate.of(2016, Month.JULY, 18);
			LocalDate localDate2 = LocalDate.of(2016, Month.JULY, 20);
			Period period = Period.between(localDate1, localDate2);
			System.out.println(period);
			
			period = Period.of(1, 2, 3);
			System.out.println(period);
			
			period = Period.ofYears(1);
			System.out.println(period);
		// ******
		
	}

}
