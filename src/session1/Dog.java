package session1;

public class Dog {
   String raza;
   int edad;
   String color;

   void ladrar() {
	   System.out.println("El " + raza + " dice: Guao Guao");
   }

   void hambriento() {
	   System.out.println("El " + raza + " dice: Grrrrrrr");
   }

   void durmiendo() {
	   System.out.println("El " + raza + " dice: ZZZZZZZZ");
   }
}