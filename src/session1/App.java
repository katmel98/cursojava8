package session1;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // System.out.println( "Hello World!" );
        
        Car volvo = new Car(2017, "Volvo S40", "Black");
        Car myCar = new Car();
        
        System.out.println("Coche A -> Año: " + myCar.year + ", Modelo: " + myCar.model + ", Color: " + myCar.color);
        System.out.println("Coche B -> Año: " + volvo.year + ", Modelo: " + volvo.model + ", Color: " + volvo.color);        
    }
}
