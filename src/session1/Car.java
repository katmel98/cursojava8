package session1;

public class Car {
	
	int year;
	String model;
	String color;
	
	Car(){
		year = 2018;
		model = "Default Car";
		color = "Blue";
	}
	
	Car(int y, String m, String c){
		year = y;
		model = m;
		color = c;
	}

}
